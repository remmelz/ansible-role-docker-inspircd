#!/bin/bash

##############################
# Start ZNC
##############################

while true; do

  su _znc -c '/usr/bin/znc -f --datadir=/var/lib/znc'

  echo "[Ok]"
  sleep 10
done

