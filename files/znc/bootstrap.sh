#!/bin/bash

###################
apt-get -y update && \
apt-get -y install procps znc && \
###################
usermod -s /bin/sh _znc && \
mkdir -p /var/lib/znc/configs && \
mv -v /var/tmp/znc.conf /var/lib/znc/configs/znc.conf && \
chown -vR _znc. /var/lib/znc/ && \
exit
