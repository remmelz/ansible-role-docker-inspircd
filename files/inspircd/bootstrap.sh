#!/bin/bash

###################
apt-get update
apt-get -y upgrade
apt-get -y install inspircd anope procps
###################
usermod -s /bin/sh irc
mkdir -p /var/run/inspircd
chmod 775 /var/run/inspircd
chown root.irc /var/run/inspircd
mv -v /var/tmp/inspircd_inspircd.conf /etc/inspircd/inspircd.conf
mv -v /var/tmp/inspircd_links.conf /etc/inspircd/links.conf
mv -v /var/tmp/inspircd_modules.conf /etc/inspircd/modules.conf
mv -v /var/tmp/anope_services.conf /etc/anope/services.conf
mv -v /var/tmp/anope_operserv.conf /etc/anope/operserv.conf
chmod 640 /etc/anope/*
chown root.irc /etc/anope/*
chown irc.irc /etc/inspircd/*
###################
exit

